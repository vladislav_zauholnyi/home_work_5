package app.controllers;

import app.models.User;
import app.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MainController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping("/users/{id}")
    public User getUserById(@PathVariable("id") long id) {
        return userRepository.findById(id);
    }

    @PostMapping("/users/{name}")
    public User addNewUser(@PathVariable("name") String name) {
        User user = new User();
        user.setName(name);
        userRepository.save(user);
        return user;
    }

    @PutMapping("/users/{id}/{name}")
    public User updateUser(@PathVariable("id") long id, @PathVariable("name") String name) {
        User user = userRepository.findById(id);
        user.setName(name);
        userRepository.save(user);
        return user;
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable("id") long id) {
        userRepository.deleteById(id);
    }
}
