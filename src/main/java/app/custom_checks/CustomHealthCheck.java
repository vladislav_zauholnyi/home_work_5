package app.custom_checks;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@Endpoint(id = "custom")
public class CustomHealthCheck {

    @ReadOperation
    public Health health() {
        String appName = "THIS IS A CUSTOM APP NAME";

        int errorCode = checkHealth();
        if (errorCode != 1) {
            return Health.down().withDetail("Error Code", errorCode).withDetail("App name", appName).build();
        }
        return Health.up().withDetail("App name", appName).build();
    }

    private int checkHealth() {
        //logic for checking health
        //if health is ok return 1
        //else return 0
        return 1;
    }
}

